const search = document.getElementById('search');
const matchList = document.getElementById('match-list');
// function is not bugged 
const searchStates = async searchText => {
  // fetch is working and also async 
  const res = await fetch('../data/state.json');
  const states = await res.json();
  let matches = states.filter(state => {
    const regex = new RegExp(`^${searchText}`, 'gi')
    return state.name.match(regex) || state.abbr.match(regex);
  });
  if (searchText.length === 0) {
    matches = [];
    // console.log("if is working")
    // console.log(matches)
    matchList.innerHTML = '';
    // confirm(matchList.innerHTML)
  }
  else{
    // console.log("else if is working")
    // console.log(outputHtml(matches))
    outputHtml(matches);}
};

const outputHtml = matches => {
  // bug is here
  // console.log(matches)
  // console.log(matches.length)
  if (matches.length) {
    const div_body = matches.map(matched =>`<div class="card card-body mb-1">
    <h4> ${matched.name} ${matched.abbr}
    <span class="text-primary">${matched.capital}</span><small> Lat: ${matched.lat} / Long: 
    ${matched.long}</small>
    </h4></div>
    `).join('');
    matchList.innerHTML = div_body;
// bugged
    // const html_val = matches.map(match => `
    // <div class="card card-body mb-1">
    //   <h4> ${match.name} (${match.abbr}) <span class="text-primary">${
    //     match.capital
    //   }</span></h4>
    //   <small>Lat: ${match.lat} / Long: ${match.long}</small>
    // </div>
    // `).join('');
    // matchList.innerHTML = html_val;
  }
};
search.addEventListener('input', () => searchStates(search.value));